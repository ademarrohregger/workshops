Alterando para uma branch
=========================

Alternar entre branches, basta não utilizar o `-d` no `git checkout`.

Para voltar à _branch_ `master`, então:

```console
$ git checkout master
Switched to branch 'master'
```

Agora, `commits` feitos voltarão a se aplicar à _branch_ `master` apenas e
portanto o HEAD passa a apontar para o último commit do `master`.
