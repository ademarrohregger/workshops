Versionando um projeto
======================

Nesta seção, você irá criar e versionar seu primeiro projeto com Git.

Ao final dela, a pasta do seu projeto deverá ficar da forma:

```
sample-repo
├── readme.md
└── sample.py

0 directories, 2 files
```

E a saída de `git log` deve ser:

```
commit 74192e77bc55edc8a0e5c98c2acb0c0d723a87fa (HEAD -> master)
Author: Seu Nome <seu@email>
Date:   Data do commit

    Add code to sample.py.

commit cdc827345951ec9e0eba6f2f2fc917d9eda89505
Author: Seu Nome <seu@email>
Date:   Data do commit

    First commit.
```

Comandos úteis
--------------

### Ver conteúdo de uma pasta

```console
$ ls
readme.md  sample.py

$ ls -lh   # -l: mostrar em lista; -h: mostrar peso em K, M, G
total 8.0K
-rw-r--r-- 1 user user 100 Feb 14 07:45 readme.md
-rw-r--r-- 1 user user  29 Feb 14 13:16 sample.py

$ ls <pasta específica> # Para buscar em uma pasta específica que não seja a
                        # atual
```

### Trocar de pasta

```console
$ cd <pasta>
```

Retornar à pasta anterior:

```console
$ cd ..
```
