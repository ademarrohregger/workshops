Iniciando um projeto
====================

```console
$ git init sample-repo   # Cria a pasta "sample-repo" como um repositório git
$ cd sample-repo         # Troca para a pasta recém criada
```

Se a pasta do projeto já existe e você quer apenas começar a versioná-lo
(transformá-lo em um repositório git), basta ir para a pasta e rodar `git
init`:

```console
$ cd pasta/do/projeto
$ git init
```
