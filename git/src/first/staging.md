Staging area
============

É a área intermediária em que as alterações são gerenciadas antes de um
_commit_ ser finalizado.

```mermaid
graph TD;
    W["Working Directory"]
    S["Staging Area"]

    W-- git add -->S
    S-- git reset HEAD -->W
    ;
```

Verificar estado do repositório
-------------------------------

```console
$ git status
```

Adicionar alterações à Staging Area
-----------------------------------

```console
$ git add <arquivos/pastas>...
```

Entendendo o `git status`
-------------------------

```console
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   readme.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        sample.py
```

### "On branch master"

Em qual ramo do programa você se encontra no momento (nesse caso, `master`).

### "Changes to be commited:"

Quais alterações estão na Staging Area.

### "Untracked files:"

Arquivos que não estão sendo versionados no projeto (ou seja, Git não se
importa com alterações neles).
