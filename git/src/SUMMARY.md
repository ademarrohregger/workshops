# Summary

[O Sistema Git](./about.md)

[Recado importante](./important-note.md)

- [Versionando um projeto](./first/about.md)
    - [Iniciando um projeto](./first/git-init.md)
    - [_Staging area_](./first/staging.md)
    - [Versionando alterações](./first/git-commit.md)
    - [Logs](./first/git-log.md)
- [Ramificando um projeto](./branch/about.md)
    - [Criando uma branch](./branch/creating.md)
    - [Alterando para uma branch](./branch/changing.md)
    - [Aplicando alterações de outra branch](./branch/git-merge.md)
