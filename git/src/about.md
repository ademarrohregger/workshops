O Sistema Git
=============

Git é um sistema de versionamento de software criado em 2005 por [Linus
Torvalds](https://github.com/torvalds) para gerenciar o desenvolvimento do
_kernel_ Linux.

Descrições bem detalhadas estão disponíveis no [site
oficial](https://git-scm.com/about).

Git NÃO é Github!
-----------------

- Git é um sistema de versionamento;
- Github é um site, e nele você pode guardar repositórios Git.

Recursos
--------

### Branches

É possível criar diferentes linhas de desenvolvimento para organizar um
projeto da maneira que julgar melhor. Um exemplo de organização pode ser: em
uma linha ficam todas as alterações já prontas para distribuição, em outra
ficam as _features_ ainda não terminadas, e a partir desta outras ramificações
são criadas, cada uma representando uma feature a ser adicionada.

### Merge

Assim como é possível ramificar a produção do software, é também possível
trazer as alterações de uma ramificação para outra, o que é feita rapidamente
com Git.

### Git é rápido

Git consegue ser de até duas ordens de magnitude mais rápido que uma de suas
antigas alternativas, SVN.

### Distribuído

É possível ter _mirrors_ de um projeto que sejam versionados independentemente
e transferir alterações de um para o outro. Isso serve, inclusive, para
permitir backups distribuídos.

### Segurança de Dados

Git faz uso de criptografia de forma que seja impossível fazer alterações sem
que elas passem despercebidas, de forma que você esteja seguro de que o projeto
está como esperado.

### Staging Area

É possível selecionar quais alterações serão versionadas e ainda fazer pequenas
adaptações antes de aplicar algumas delas.

### Gratuito e Open-Source

O título já diz tudo.
