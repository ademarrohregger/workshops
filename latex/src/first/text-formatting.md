Formatação de texto
===================

Em LaTeX, é possível formatar texto com macros como `\textbf`, `\textit`,
`\texttt`, etc. Exemplo:

```latex
Deixe seu texto em \textbf{Negrito}, \textit{Itálico} ou \texttt{Monoespaçado}
("TeleType").
```

O resultado disso é:

>Deixe seu texto em **Negrito**, _Itálico_ ou `Monoespaçado` ("TeleType").

<warn title="Aspas">
Prefira escrever: <code>``texto''</code> em vez de `"texto"`, assim LaTeX irá
fazer a curvatura das aspas obedecer o início e fim do texto entre aspas, como
no exemplo abaixo:
![Aspas](./img/example-quotes.png)
</warn>

<warn title="Ênfase">
Quando precisar dar ênfase em um trecho, prefira utilizar `\emph`, assim o
compilador se adapta ao estilo de ênfase melhor compatível com o contexto (se
estiver em um ambiente já em negrito, ou já em itálico, ou dependendo de como a
classe do documento estabelece ênfase...).
</warn>
