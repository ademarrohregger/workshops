Preâmbulo: pacotes
==================

<concept title="Pacotes">
São códigos LaTeX que disponibilizam macros e ambientes úteis para seu projeto,
semelhante a bibliotecas em programação.
</concept>

Incluindo pacotes
------------------

Um pacote pode ser incluído com:

```latex
\usepackage{<nome-do-pacote>}
```
