Listagem
========

Há 3 formas de listagem padrões, todas elas definidas com um ambiente
específico para cada e utilizando `\item` para definir um novo item na
listagem.

Itemização
----------

Utilizando o ambiente `itemize`, a listagem é feita com "Bullets":

```latex
\begin{itemize}
    \item Primeiro item;
    \item Segundo item;
    \item Terceiro item;
    \item Último item.
\end{itemize}
```

Produzirá:

- Primeiro item;
- Segundo item;
- Terceiro item;
- Último item.

Enumeração
----------

Utilizando o ambiente `enumerate`, a listagem é feita com números:

```latex
\begin{enumerate}
    \item Primeiro item;
    \item Segundo item;
    \item Terceiro item;
    \item Último item.
\end{enumerate}
```

Produzirá:

1. Primeiro item;
2. Segundo item;
3. Terceiro item;
4. Último item.

Descrição
---------

Utilizando o ambiente `description`, com o auxílio de `[texto]` após cada
`\item`, o que estiver entre `[]` ficará em negrito:

```latex
\begin{description}
    \item [Primeiro item]: Descrição grande o suficiente para caber em mais de
        uma linha, assim é possível ver como funciona a indentação do
        \texttt{description}.
    \item [Segundo item]: Descrição.
    \item [Terceiro item]: Descrição.
    \item [Último item]: Descrição.
\end{description}
```

Produzirá:

![Exemplo de `description`](./img/example-description.png)

Sublistas
---------

Sublistas podem ser feitas abrindo uma lista no `\item` de outra:

```latex
\begin{itemize}
    \item Primeiro item:
        \begin{itemize}
            \item Que contém subitens.
        \end{itemize}
\end{itemize}
```

Produzirá:

- Primeiro item:
    - Que contém subitens.
