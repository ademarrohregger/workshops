A Web
=====

A Web trabalha em cima do protocolo TCP, em um modelo que chamamos de
"Cliente-Servidor". A ideia é, justamente, que um Cliente mande requisições ao
Servidor, e o Servidor as atenda (possivelmente com alguma resposta).

O que é um servidor?
--------------------

De maneira simples, um servidor acaba sendo um **processo** rodando em uma
**porta**. Um **processo** é simplesmente um programa de computador qualquer, e
uma **porta** é um número de 16 bits na qual um processo pode se registrar e se
comunicar com outros processos (seja na rede local ou na externa).

Por exemplo: quando você, em seu navegador, acessa `http://google.com`, o que o
navegador fará é, na verdade, enviar uma requisição para
`http://google.com:8080`, ou seja: a porta 8080 do servidor no endereço
`google.com`. No servidor, existe algum programa rodando que está ouvindo a
porta 8080, e portanto vai perceber que alguma requisição foi enviada a ela,
tratar essa requisição, e emitir uma resposta por essa mesma porta.

Como se sabe quem é `google.com`?
---------------------------------

De alguma maneira, os dados que seu navegador envia de `google.com` precisam
chegar até algum computador (onde está o servidor). Mas como se sabe que
computador é esse? Para isso existem várias formas de endereçamento.

### DNS

Para o modelo de camadas da Internet, "google.com" não é um endereço.
Se você voltar à tabela das camadas no [1º tópico](./a-internet.md), vai ver
que o modelo Internet trabalha com endereços IP (para a camada de Rede) e MAC
(para a camada de Enlace). O que se pode dizer é que "google.com" é um nome de
um domínio, e nomes são resolvidos através de um DNS (Domain Name System). Um
DNS é um sistem que possui tabelas que relacionam nomes de domínios a endereços
IP, por exemplo: mapeando `google.com` para 172.217.30.99. Assim, o que seu
navegador faz é:

1. Ele tem registrado o IP de alguns servidores de DNS nos quais ele confia
   (por exemplo, dois servidores de DNS da Google ficam em 8.8.8.8 e 8.8.4.4);
2. Quando ele precisa descobrir qual o IP de algum nome de domínio (ex:
   `google.com`), ele pergunta a algum dos servidores de DNS: "qual é o IP
   deste nome?";
3. Caso o servidor de DNS conheça aquele nome, ele responde ao seu navegador
   com o IP correspondente;
4. Caso o servidor **não** conheça, ele responde: "eu não conheço o nome que
   você pediu, mas este outro servidor DNS pode conhecer", e então o pedido de
   "qual o IP" é feito para esse outro servidor.

### IP e MAC

Tendo o IP do servidor em mãos, ele será utilizado para direcionar os pacotes
até a rede correta. A questão é como enviar para o **dispositivo** correto.
Primeiramente é preciso entender que cada dispositivo tem a possibilidade de
ter 2 IPs: um público (que é o que as redes externas enxergam) e um para a rede
local. O IP público é fornecido pelo seu provedor de internet (ISP - _Internet
Service Provider_) quando você estabelece uma conexão com a internet. Jà o IP
local (iniciado em 192.168) pode ser definido de duas formas:

- Por você manualmente configurando o seu sistema para pegar um mesmo IP local
  sempre (um **IP Fixo**), o que dificilmente é feito por demandar que seja
  administrado manualmente, podendo ainda gerar conflitos de IP (dois
  dispositivos com o mesmo IP).
- Pelo seu roteador dinamicamente distribuindo IPs locais (ex: o 1º computador
  a se conectar recebe o IP 192.168.0.2, o 2º computador recebe o IP
  192.168.0.3, e por aí vai), o que é feito por um sistema chamado **DHCP**.

Quando um pacote é enviado de uma rede externa para a rede local, ele indica o
destino pelo IP público do dispositivo. Quando o pacote chega na rede local, se
faz uma busca por qual dispositivo possui o IP de destino, e o resultado dessa
busca é o endereço MAC dele. O MAC é o que identifica unicamente um dispositivo
(e, portanto, se espera que cada dispositivo seja o único possuidor de um
determinado MAC). Tendo o MAC do dispositivo de destino, o roteador escreve
esse endereço no campo de destino do pacote e o envia, e com isso se completa o
recebimento de um pacote.

Isso é o que acontecerá na rede local do servidor: algum roteador da rede em
que ele está receberá os pacotes da sua requisição, esse roteador irá buscar
qual o MAC do servidor, e então finalmente enviará o pacote a ele. Ao receber
os pacotes, o servidor irá tratá-los e enviar novos pacotes a você como
resposta, e aí se repete o mesmo processo de se enviar ao servidor (com exceção
da parte de DNS, afinal o IP do remetente de um pacote é guardado nos
meta-dados desse pacote, então o servidor já sabe para qual IP enviar a
resposta).
