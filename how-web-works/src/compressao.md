Suporte a Compressão
====================

A parte de compressão é ligeiramente mais complicada. Podemos trabalhar com
diferentes métodos de compressão definindo o que queremos pelo _header_
`Accepted-Content` da requisição, e podemos confirmar se o conteúdo foi
fornecido no método esperado analisando o _header_ `Content-Encoding` da
resposta. Iremos lidar com os seguintes valores possíveis de métodos de
compressão:
- `gzip`: compressão do [gzip](https://en.wikipedia.org/wiki/Gzip);
- `deflate`: compressão da [zlib](https://en.wikipedia.org/wiki/Zlib);
- `br`: compressão específica da Google,
  [brotli](https://github.com/google/brotli).

O `gzip` e `zlib` estão disponíveis na biblioteca padrão:

```python
# (Adicione aos imports)
import gzip
import zlib
```

Já a `brotli` é uma biblioteca externa, então precisamos instalar a
dependência. Se você criou o projeto usando Poetry, aqui vem uma vantagem de se
utilizar um gerenciador de dependências: apenas edite seu `pyproject.toml`
adicionando `brotli` como dependência:

```toml
...

[tool.poetry.dependencies]
python = "^3.7"
brotli = "^1.0"

...
```

Execute `poetry install` e pronto, sua nova dependência está instalada na
_virtualenv_ do projeto e pode ser importada no `simplehttp.py`:

```python
import brotli
```

Com as 3 libs de compressão importadas, faremos um dicionário de métodos de
compressão para suas respectivas funções que, a partir de uma sequência de
`bytes` comprimida, retornam uma nova sequência, porém descomprimida:

```python
# (Sugestão: adicione antes de `fetch_response`)
DECOMPRESSORS: Dict[str, Callable[[bytes], bytes]] = {
    'gzip': gzip.decompress,
    'deflate': zlib.decompress,
    'br': brotli.decompress,
}
```

Em `fetch_response`, logo após recebermos os dados da resposta (`body`),
verificamos se o `Content-Encoding` está definido. Se estiver, então algum
método de compressão foi aplicado e precisamos descomprimi-lo. Agora como
encaixar cada método para cada valor possível? Aí que entra o motivo de
utilizarmos um dicionário: nosso dicionário `DECOMPRESSORS` mapeia um valor
possível de `Content-Encoding` para uma função de descompressão, então podemos
pegar direto esse valor como chave do dicionário. Isso irá nos devolver a
função correta, então a chamamos passando o `body` como objeto a ser
descomprimido e pronto, temos nosso `body` correto:

```python
    # (Adicione abaixo de `body = fetch_body(io, headers)` em `fetch_response`)

    if 'Content-Encoding' in headers:
        assert body is not None
        body = DECOMPRESSORS[headers['Content-Encoding']](body)
```

E pronto, está finalizado o nosso suporte a compressão de dados, e com isso
finalizamos nossa biblioteca!
