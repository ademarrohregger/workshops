Refinando a Implementação
=========================

Como comentado anteriormente, nossa implementação ainda não suporta nem
compressão nem criptografia com HTTPS. A parte de compressão serve para
otimizar o uso do tráfego de rede, precisando enviar menos dados (apesar de que
quem recebe esses dados ainda precisa descomprimí-los). Já a parte de
criptografia serve para que os dados trafeguem de maneira segura através de
[TLS](https://pt.wikipedia.org/wiki/Transport_Layer_Security) (que sucede o
mecanismo anterior, SSL, então é comum encontrar pessoas falando de SSL mas se
referindo a TLS). Além disso, com TLS é possível garantir que o servidor com
que se está comunicando é realmente o que se espera.

Nosso passo então é implementar essas duas funcionalidades restantes.
