Enviando uma requisição simples
===============================

Preparações
-----------

### Um tipo `Request`

Antes de tudo, criaremos uma forma de agrupar os _headers_ e dados de uma
requisição de uma forma adequada para nossa biblioteca. Essa forma será através
de uma `dataclass` que chamaremos de `Request`:

```py
# (Insira acima do `if __name__ == ...`)
@dataclass(frozen=True)
class Request:
    headers: Headers = field(default_factory=lambda: {})
    data: Optional[str] = None
    method: str = 'GET'
```

Para poder utilizar a anotação `@dataclass` e o `field`, precisamos importá-los
do pacote `dataclasses`:

```python
# (Deixe logo abaixo do `import sys`)
from dataclasses import dataclass, field
```

Perceba que `Headers` não foi definido, e portanto precisamos defini-lo. Ele
`Headers` servirá para definir os _headers HTTP_, que são um conjunto de
combinações `chave: valor`, sendo assim, podemos defini-lo como um dicionário
que mapeia `string` -> `string`:

```python
# (Insira acima da definição do `Request`)
Headers = Dict[str, str]
```

Por fim, falta definir `Dict` e `Optional`. Ambos vêm do pacote `typing`, e
podem ser importados da forma:

```python
# (Insira também abaixo do `import sys`)
from typing import Dict, Optional
```

### Explicando o `@dataclass`

A anotação `@dataclass` permite definir de maneira rápida uma classe e quais
seus atributos. Por baixo dos panos, isso irá gerar uma classe que já possui um
construtor recebendo `headers`, `data` e `method`. Em uma `dataclass`, cada
atributo é definido da forma `nome: tipo = valor_padrão`, então:
- `headers` é do tipo `Headers` e, por padrão, é criado como um dicionário
  vazio. O `field(...)` é necessário porque se fizéssemos:

  ```python
  headers: Headers = {}
  ```

  Pela forma como objetos se comportam em Python quando usados como valores
  padrões para atributos ou parâmetros de funções, todos os `headers`
  compartilhariam um mesmo dicionário (a menos que fosse passado um valor
  específico para `headers` ao construir um objeto `Request`), e não é isso que
  queremos. O `field(...)` contorna isso construindo um dicionário diferente
  para cada `Request`;
- `data` é do tipo `Optional[str]`. `Optional` define que ou o valor dele é do
  tipo definido entre os `[]`, ou ele é `None`. Por padrão, `data` é `None`;
- `method` é uma `str` e, por padrão, é `'GET'`.

O `frozen=True` faz com que objetos dessa classe sejam imutáveis, ou seja: é
impossível fazer `obj.data = <algo>`. Assim, uma vez um `Request` criado com
determinados valores, ele terá sempre aqueles mesmos valores.

Uma função para enviar requisições
----------------------------------

### Definindo a função

Para enviar uma requisição, criaremos uma função `send_request(io, request)`,
em que:
- `io` será um IO utilizado para escrever os dados da requisição. Operações de
  IO são as que lidam com escrita/leitura em dispositivos externos, como seu
  HDD/SSD, rede, ..., ou seja: qualquer coisa que não seja a memória ou
  registradores do seu processador. Veremos como criar um IO a partir de um
  `socket` na hora de integrar esta função ao nosso `__main__`.
- `request` será um `Request , contendo então os _headers_, método e os dados
  da requisição.

```py
# (Adicione após a definição de `Request`)
def send_request(io, request):
    pass
```

### Adicionando anotações de tipo

Para deixar nossa função mais legível e ainda aproveitar ferramentas que
validam se nosso código está correto, é interessante indicarmos qual deveriam
ser os tipos dos atributos de nossa função. Isso é feito mudando cada parâmetro
para `nome_do_parametro: TipoEsperado`. Nosso `io` será um `IO[bytes]`, pois as
operações de IO com `socket` exigem que sejam passados a ele _bytes_, e não
strings normais, enquanto o tipo de `request` será o nosso dataclass `Request`:

```python
# (Altere a definição de `send_request` com as anotações de tipo)
def send_request(io: IO[bytes], request: Request):
    pass
```

O `IO` deve ser importado de `typing` também:

```python
# (Altere a linha `from typing import [...]` adicionando `IO` também)
from typing import Dict, IO, Optional
```

### Implementando a função

Para enviar nossa requisição podemos aproveitar que IOs, em Python, possuem as
operações `write(bytes)` e `flush()`. A ideia de `flush` é forçar que todas as
escritas sejam feitas. O motivo é que, quando se lida com IO, nem sempre os
dados são escritos diretamente no destino: como operações de IO costumam ser
lentas, geralmente se escreve em _buffer_ e _eventualmente_ o conteúdo do
_buffer_ é escrito no dispositivo.

O que precisamos agora é escrever nesse `io` o conteúdo de um request HTTP
completo. Isso significa enviar algo no formato:

```http
<método> <diretório> HTTP/<versão>
<headers>

<corpo da requisição>
```

Então o que faremos nessa função é o seguinte:
1. Escrevemos a 1ª linha da requisição considerando a versão HTTP 1.1 e o
   diretório "/" (ou seja, a raiz do site, ex: `google.com/`;
2. Inserimos cada um dos headers iterando pelo `headers` do `Request`,
   lembrando que cada header deve estar no formato `chave: valor`;
3. Pulamos uma linha e inserimos os dados (i.e. o corpo) da requisição;
4. Forçamos que tudo seja enviado usando `flush()`.

#### 1º Passo: definição da requisição

Para o 1º item, mandaremos escrever o método do `request` (que pode ser obtido
com `request.method`, como definimos na _dataclass_) seguido de `/ HTTP/1.1` e
uma quebra de linha:

```python
    # (Troque o `pass` de `send_request` por isso)
    io.write(f'{request.method} / HTTP/1.1\n')
```

O `f` serve para indicar que aquela _string_ é uma **_string_ formatada**, ou
seja: o conteúdo dela não é literalmente `{request.method} ...`, mas sim que os
`{}` devem ser substituídos pelo que há dentro deles. Ou seja, em
`f'O método é {request.method}!'`, se `request.method` for `GET`, a _string_
vai virar `'O método é GET!'`.

Só há um pequeno detalhe: o `write` de IO exige que sejam **bytes**, e não uma
_string_. Sendo assim, precisamos pegar essa _string_ e encaixá-la em
codificação de bytes (por exemplo, UTF8). Uma _string_ pode ser codificada
utilizando `uma_string.encode(codificação)`. Se `codificação` não for
fornecido, se assume o padrão (`'utf-8'`). Como o padrão já serve para todos os
casos possíveis de requisições HTTP (ou pelo menos da parte que não é de
dados), então, alteramos aquela mesma linha adicionando um `.encode()`:

```python
    # (Altere aquela mesma última linha que foi adicionada)
    io.write(f'{request.method} / HTTP/1.1\n'.encode())
```

#### 2º Passo: inserindo headers

Para inserir os _headers_, podemos aproveitar que nosso `request.headers` é um
dicionário e iterar por tuplas `(chave, valor)` diretamente usando o método
`.items()` de `dict` e, para cada um deles, escrever `header: valor` na
requisição:

```python
    # (Adicione abaixo do `io.write(f'{request.method} [...]`)
    for header, value in request.headers.items():
        io.write(f'{header}: {value}\n'.encode())
```

Assim, se os _headers_ forem `{'Content-Type': 'charset', 'Content-Length':
'2048'}`, a requisição ficará:

```python
Content-Type: charset
Content-Length: 2048
```

#### 3º Passo: inserindo dados

A parte de inserir dados é simples: apenas aproveitamos o atributo `data` de
`Request` e o escrevemos caso ele esteja definido:

```python
    # (Adicione logo abaixo do código do 2º passo)
    if request.data is not None:
        io.write(f'{request.data}\n'.encode())
```

#### 4º Passo: forçar o envio da requisição

Não há muito segredo para este passo: temos o `io` e ele possui um método
`flush()`, que força que o `write(...)` anterior seja feito, então o utilizamos
(não esquecendo de mandar uma quebra de linha extra na requisição - ela é
importante para dizer que ali acaba a requisição, conforme definição do HTTP):

```python
    # (Adicione após o `io.write(f'{request.method} [...]`)
    io.write(b'\n')

    io.flush()
```

E pronto, nossa função `send_request` está mínima para vê-la em prática. Note o
prefixo `b` antes da string `'\n'`: ele serve para o mesmo propósito do
`encode()`, porém só pode ser utilizado quando não se tratar de uma `f-string`.

#### Utilizando a `send_request` em nosso programa

Para chamar a `send_request`, precisamos criar tanto um `io` (a partir do
`socket` que criamos mais cedo) quanto uma requisição. Por enquanto, nossa
requisição será vazia, apenas indicando que o método de acesso será `GET` (que
já é padrão do nosso dataclass, então não precisamos explicitá-lo):

```python
    # (Substitua o `print('Conexão estabelecida')`)
    sock_out = sock.makefile('wb')

    send_request(sock_out, Request())
```

O `s.makefile` é responsável por gerar um `IO[bytes]` para aquele `socket`, e
nesse caso estamos indicando que a operação desse IO é `wb`, de `write` +
`binary`. Ou seja, esse `IO` será para apenas escrita de sequências de bytes.

Código final desta seção
------------------------

```python
import socket
import sys
from dataclasses import dataclass, field
from typing import Dict, IO, Optional


Headers = Dict[str, str]


@dataclass(frozen=True)
class Request:
    headers: Headers = field(default_factory=lambda: {})
    data: Optional[str] = None
    method: str = 'GET'


def send_request(io: IO[bytes], request: Request):
    io.write(f'{request.method} / HTTP/1.1\n'.encode())

    for header, value in request.headers.items():
        io.write(f'{header}: {value}\n'.encode())

    if request.data is not None:
        io.write(f'{request.data}\n'.encode())

    io.write(b'\n')

    io.flush()


if __name__ == '__main__':
    with socket.create_connection((hostname, 80)) as sock:
        sock_out = sock.makefile('wb')

        send_request(sock_out, Request())
```

O código pode ser executado com:

```console
$ python -m simplehttp google.com
```

No momento, se a princípio tudo estiver certo, nada acontecerá (nem mesmo
erros).
