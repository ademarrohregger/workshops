Implementando HTTP
==================

Partindo então para a prática, nosso objetivo será **montar uma pequena
biblioteca de requisições HTTP**, tendo também como informar qual foi a
resposta da requisição.

Por que uma biblioteca?
-----------------------

De certa forma, ter uma biblioteca ajuda a praticar a criação de um software
real. Separar projetos de software em pequenas bibliotecas independentes (em
vez de um grande código em que todas os componentes são acoplados e
interligados, ou criar separações meramente artificiais que não reduzem esse
acoplamento) tem um valor positivo para futuras manutenções, e portanto é
interessante praticar a criação de uma pequena biblioteca para já se acostumar.

O que é uma biblioteca, afinal?
-------------------------------

Sendo simples, uma biblioteca é um pacote de funcionalidades (definições de
funções, tipos, etc.) que pode ser reaproveitado em outros projetos. Por
exemplo, Python vem com uma biblioteca padrão que tem módulos como `sys` (que
fornece funções relacionadas ao SO), `path` (que fornece funções/classes para
trabalhar com caminhos, criação de pastas, etc.), e ainda há outras bibliotecas
que podem ser instaladas, como a [Pandas](https://pandas.pydata.org/) para
análise de dados.

Entendendo os exemplos de comando
---------------------------------

Os exemplos de comandos estarão no seguinte formato:

```console
$ comando a ser executado
O que é mostrado no terminal ao executar o comando acima,
podendo inclusive ter mais de uma linha.
$ outro comando a ser executado
Saída do outro comando.
```

### Comandos em pastas específicas

Caso um comando tenha que ser executado a partir de uma pasta específica, isso
será representado da forma:

```console
$ [pasta] comando
```

### Comentários

O que estiver após um `#` nos comandos serve apenas para descrever o que se
está fazendo com ele, então tudo que vier depois de `#` em uma linha **não faz
parte do comando** pode ser ignorado:

```console
$ execute isso      # mas não isso
```
