Sockets
=======

O que são Sockets
-----------------

Suponha que dois programas (chamaremos de "A" e "B") queiram se comunicar pela
rede. No lado em que A está executando, o Sistema Operacional irá gerenciar
essa comunicação representando, internamente, A e B de uma forma que faça
sentido para ele. Essa representação se chama **socket**, e o SO que gerencia A
dirá para ele que B também é um _socket_. No lado de B, o SO que gerencia B
fará a mesma coisa: dirá pra B que ele e A são _sockets_. _Sockets_, então, são
representações de cada uma das pontas de uma comunicação.

Na prática, essa representação será **um endereço IP + uma porta** e serão
repassados aos programas **como se fossem arquivos**. Ou seja: os programas
juram que estão escrevendo e lendo de arquivos, quando na verdade o tempo todo
o SO ficará traduzindo as escritas e leituras para envio/recebimento de dados
pela rede.

### Tipos de Sockets

- Sockets de Datagrama, para UDP;
- Sockets de Stream, para TCP;
- Sockets crus, para quando não se tem um protocolo específico na camada de
  Transporte.

### Operações de Sockets

As operações possíveis de se fazer com _sockets_ são:
- Conectar-se a um _socket_;
- Enviar dados;
- Receber dados;
- Encerrar uma conexão.

Sockets em Python
-----------------

Em Python, _sockets_ são disponibilizados na biblioteca `socket` (disponível na
biblioteca padrão de Python - ou seja, não é necessário instalar por fora).

Então, em nosso arquivo `simplehttp.py`, a primeira coisa que faremos é
importar essa biblioteca:

```python
import socket
```

### Criando uma conexão _socket_

A biblioteca `socket` permite criar uma conexão com a função
`create_connection(address)`, em que `address` é uma tupla `(endereço-IP,
porta)`. Sendo assim, se quisermos nos conectar à <http://google.com>, basta
adicionar uma chamada a essa função ao nosso código:

```py
with socket.create_connection(('google.com', 80)) as sock:
    print('Conexão estabelecida.') # Meramente para ver que funciona
                                   # Apagaremos logo logo
```

Perceba que chamamos a função em um bloco `with`. Esse bloco se chama [context
manager](http://book.pythontips.com/en/latest/context_managers.html), e serve
para garantir que nosso _socket_ seja fechado corretamente após trabalharmos
com ele ou caso algum erro aconteça. Por exemplo, o seguinte bloco `with` (não
acrescente isso ao seu código):

```python
with get_resource() as r:
    do_thing(r)
```

É ligeiramente equivalente a:

```python
r = get_resource() # Aloca o recurso
try:               # Pula a execução deste bloco caso aconteça algum erro
    do_thing(r)    # Trabalha com o recurso
except:            # Caso dê algum erro...
    raise          # Relança o erro
finally:           # Sempre executa, tendo entrado no `except` ou não
    r.close()      # Garante que o recurso tenha sido liberado
```

Portanto, quando ver algum código com `algo.close()`, dê uma olhada se não é
bom substituí-lo por um bloco `with`.

Até o momento, nosso código então é:

```python
import socket

with socket.create_connection(('google.com', 80)) as sock:
    print('Conexão estabelecida.')
```

### Um pouco de organização

Em Python, todo módulo pode funcionar como:
- Uma biblioteca (ou seja, sendo importado por outros módulos);
- Um aplicativo.

Nossa biblioteca precisa poder saber quando ela está sendo importada como
biblioteca e quando ela está sendo executada como um aplicativo.

Quando você executa no terminal:

```console
$ python modulo.py # Ou ainda: python -m modulo
```

Python faz atribui a uma propriedade "mágica" `__name__` daquele módulo o valor
`'__main__'`. Então, para saber se nosso módulo está sendo executado como um
aplicativo (e não sendo importado por outro módulo), basta compararmos se essa
propriedade possui esse valor, e com isso evitamos aquela criação de _socket_
conectando com Google aconteça simplesmente porque alguém deu `import
simplehttp`. Portanto, nosso código ficará:


```py
import socket

if __name__ == '__main__':
    with socket.create_connection(('google.com', 80)) as sock:
        print('Conexão estabelecida.')
```

Assim, `import simplehttp` não executa aquela criação de _socket_, apenas um
`python simplehttp.py`.

Recebendo o _host_ e porta pela linha de comando
------------------------------------------------

Para que nossa biblioteca fique um pouco mais robusta e não ter o endereço que
queremos acessar secos no código, podemos fazer com que o usuário possa enviar
esse valor ao executar o programa:

```console
$ python -m simplehttp google.com
```

Para isso, podemos aproveitar a variável `argv` do pacote `sys`. Ela contém uma
lista com todos os argumentos enviados pela linha de comando, sendo que no
índice 0 (`argv[0]`) sempre fica qual o programa sendo executado. Então,
podemos importar `argv` e trocar `'google.com'` pelo valor de `argv[1]`:

```python
# (Adicione abaixo de `import socket`)
import sys

# (Adicione acima do bloco `with`)
hostname = argv[1]

# (Substitua `'google.com'` por `hostname` na linha do bloco `with`)
with socket.create_connection((hostname, 80)) as sock:
```

Código final desta seção
------------------------

```py
import socket
import sys

if __name__ == '__main__':
    with socket.create_connection((hostname, 80)) as sock:
        print('Conexão estabelecida.')
```

O código pode ser executado com:

```console
$ python -m simplehttp google.com
```
