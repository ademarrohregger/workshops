Validando respostas
===================

Preparações
-----------

### Um tipo `Response`

Da mesma forma que fizemos um `Request` para requisições, faremos um `Response`
para tratar as respostas recebidas pelo servidor. Uma resposta HTTP é
constituída por:

- Código da resposta (200, 404, etc.);
- _Headers_;
- Corpo da resposta (dados).

Assim como para requisições, o corpo da resposta é opcional. Sendo assim, a
nossa definição de `Response` pode ser:

```python
# (Adicione abaixo da definição de `Request`)
@dataclass(frozen=True)
class Response:
    code: int
    headers: Headers
    data: Optional[str]
```

Uma função para tratar respostas
--------------------------------

Esta função será bem mais complexa do que a para enviar requisições, pois
quando enviamos uma requisição, sabemos exatamente como vai ser o formato dela
em HTTP, isso inclui saber quando começam os _headers_, quando começam os dados,
e por aí vai. Porém, quando estamos **recebendo** uma resposta, não temos essas
informações de antemão, precisando então ir "entendendo" os trechos da resposta
de pouco em pouco. Então, de maneira geral, iremos separar nosso tratamento de
respostas em quatro etapas:
1. A aquisição do código da resposta;
2. O recebimento exclusivamente dos _headers_;
3. O recebimento exclusivamente dos dados;
4. A detecção de qual a codificação utilizada na _string_ de resposta, para
   então decodificá-la.

### 1º Passo: o código da resposta

Podemos começar colocando a nossa `fetch_response` para ler a 1ª linha da
requisição, que estará no formato `HTTP/1.1 <código> <descrição do código>`.
Como estamos só interessados no código (que será um inteiro), podemos usar
`split()` de _strings_ para transformar a linha em uma lista `['HTTP/1.1',
'<código>', '<descrição do código>']`, e então pegar apenas o 2º elemento da
lista (ou seja, o de índice 1) e convertê-lo para um inteiro:

```python
# (Adicione abaixo da definição de `Response`)
def fetch_response(io: IO[bytes]) -> Response:
    code =  int(io.readline().split()[1])
```

O `io.readline()` nos dá uma linha da requisição. Isso será
importante para validar os _headers_.


### 2º Passo: recebimento dos _headers_

Para o recebimento dos _headers_, criaremos uma função `read_headers(io)`.
Esse `io` será semelhante ao do `send_request`, porém será um IO de **leitura**
(e não de escrita). A assinatura dessa função será:

```python
# (Adicione acima da definição de `fetch_response`)
def read_headers(io: IO[bytes]) -> Iterable[Tuple[str, str]]:
```

Ou seja, uma chamada de `read_headers` retornará um iterável em que cada
elemento é uma tupla `(header, valor)` (ex: `('Content-Type', 'text/html')`).
Como foi dito anteriormente, não sabemos de antemão quando acabam os _headers_,
então nossa estratégia será ficar vasculhando todas as linhas que recebermos e,
enquanto não vier uma linha em branco ou contendo apenas espaços, consideramos
tudo como _header_:

```python
    while True:
        line = io.readline().strip()
        if not line:
            return
```

Com isso nossa função `read_headers(io)` ficará em um _loop_ e só vai parar
quando a linha não tiver algum conteúdo (para isso o `if not line`). Perceba
que é feito um `strip()` de `io.readline()`. Isso nada mais é do que para
remover espaços em excessos no início e no fim da linha.

O próximo passo é separar o que é o nome do campo do _header_ e o que é o valor
dele. Como os _headers_ no HTTP obedecem o formato `header: valor`, podemos
aplicar um `split` passando `': '` (perceba o espaço após o `:`) como
separador, e que só o consideraremos como separador para o `split` uma única
vez (assim algo como `campo: valor com : no meio` não irá separar em 3 strings
diferentes, e sim em duas: `'campo'` e `'valor com : no meio'`).

```python
        key, value = line.decode().split(': ', maxsplit=1)
        yield key, value
```

O `yield` serve para que nossa função, em vez de retornar um único valor e
parar de executar, ela apenas se torne um
[generator](https://wiki.python.org/moin/Generators). Assim, fica possível
iterar por essa função, por exemplo:

```python
for key, value in read_headers(io):
    print(f'Found {key} with value {value}')
```

A vantagem de um _generator_ é que os valores retornados por ele função são
gerados sob demanda, então se for percorrido apenas metade dos _headers_, a
outra metade não será processada e assim economizamos recursos.  Perceba também
o uso do `decode`, afinal da mesma forma que escrevemos `bytes` pelo IO, os
valores lidos também estão em `bytes`, porém queremos eles como _strings_.

Nossa função `read_headers`, então, ficará:

```python
def read_headers(io: IO[bytes]) -> Iterable[Tuple[str, str]]:
    while True:
        line = io.readline().strip()
        if not line:
            return

        key, value = line.decode().split(': ', maxsplit=1)
        yield key, value
```

Para finalizar a parte de leitura de _headers_, essa função pode ser utilizada
em uma mais geral que apenas retorna essas tuplas `(header, valor)` como um
`Headers`:

```python
# (Adicione depois da definição de `read_headers`)
def fetch_headers(io: IO[bytes]) -> Headers:
    return dict(read_headers(io))
```

Com essas duas funções prontas, podemos incrementar a `fetch_response` (a
função principal para tratar o recebimento de respostas) adicionando uma
chamada para `fetch_headers`:

```python
def fetch_response(io: IO[bytes]) -> Response:
    code =  int(io.readline().split()[1])

    headers = fetch_headers(io)
```

Com isso já temos o código e os _headers_ da resposta do servidor (você pode
printá-los caso queira ver o resultado até o momento).

### 3º Passo: recebimento dos dados

O recebimento dos dados será a parte mais complexa do tratamento de respostas.
Primeiramente porque há, sem ir muito além, duas principais formas de o
conteúdo ser fornecido:

1. Em texto-plano (sem o _header_ `Content-Encoding`), que apesar de ser mais
   portável não faz um uso tão bom da rede;
2. Em algum formato comprimido (`Content-Encoding: gzip`, por exemplo), que já
   usa melhor a banda da rede uma vez que são necessários transmitir menos
   bytes - porém fica a cargo do requisitante descomprimir o conteúdo.

Além disso, ainda há a possibilidade de:
- Estar definido o _header_ `Content-Length`, informando o tamanho desse
  conteúdo (tendo um valor numérico);
- Ou, no lugar do `Content-Length`, estar definido `Transfer-Encoding` como
  `chunked`, simbolizando que a que a transferência será em _chunks_ (ou seja,
  em pedaços separados);
- O conteúdo estar em uma codificação diferente (por exemplo, `iso-8859-1` em
  vez de `utf-8`). Essa parte também será resolvida no 4º passo.

Então será necessário lidar com todos esses casos, e resolveremos
especificamente as questões de _encoding_ no passo 4.

#### Leitura principal

Para este 3º passo criaremos uma função `fetch_body(io, headers)`. O `headers`
será necessário aqui pois, como dito nos itens anteriores, eles tem mudanças em
como serão tratados os dados e portanto precisam estar previamente carregados
para utilizarmos. Definimos então a assinatura da função:

```python
# (Adicione acima da definição de `fetch_response`)
def fetch_body(io: IO[bytes], headers: Headers) -> Optional[bytes]:
```

Perceba o uso de `Optional` no retorno, afinal (lembrando da definição de uma
resposta HTTP) o corpo da requisição é opcional.

A primeira questão que deve ser resolvida agora é se o conteúdo que está sendo
enviado já possui um tamanho pré-determinado, e isso podemos fazer verificando
se existe o _header_ `Content-Length`:

```python
    # (Adicione como corpo da implementação de `fetch_body`)
    if 'Content-Length' in headers:
        length = int(headers['Content-Length'])
        return io.read(length)
```

Ou seja: se o _header_ está definido, então o convertemos para `int` e passamos
como argumento para o `io.read`. Isso vai fazer com que sejam lidos `length`
bytes da resposta, sendo exatamente o tamanho do conteúdo.

Caso não tenha o _header_ definido (perceba que **não** precisamos de um
`else`, afinal se entrar no `if` a função cai em um `return`), o próximo passo
é verificar se o método de transferência não é `chunked`. Se for, então
carregamos o conteúdo usando uma função especial `get_chunked_body`, que ainda
iremos criar:

```python
    if (
        'Transfer-Encoding' in headers and
        headers['Transfer-Encoding'] == 'chunked'
    ):
        return get_chunked_body(io)
```

Por fim, para simplificar, partiremos do princípio de que, se não sabemos nem o
tamanho do conteúdo nem o método de transferência, então não havia conteúdo
para ser carregado:

```python
    return None
```

#### Leitura em _chunks_

Tendo então finalizado a `fetch_body`, o próximo passo é definir a
`get_chunked_body` utilizada nela. A ideia dessa função é retornar uma
sequência de bytes sem quando o formato de transferência é `chunked`, então a
definição dela será:

```python
def get_chunked_body(io: IO[bytes]) -> bytes:
    b''.join(read_chunks(io))
```

A estratégia aqui será bastante semelhante a como fizemos com _headers_:
`read_chunks` será iterável nos dando cada um dos _chunks_ recebidos, e isso
tudo será unido em uma única sequência de bytes (`b''.join(container)` junta
todos os elementos de `container` em um só sem inserir algo extra entre eles).
A `read_chunks` terá o seguinte início:

```python
def read_chunks(io: IO[bytes]) -> Iterable[bytes]:
    while True:
        line = io.readline().strip().decode()
```

Para continuar a partir daqui, precisamos entender como os chunks são
transferidos. Em resumo, eles são enviados em pares de linhas, em que:
1. Contém o tamanho do _chunk_ **em hexadecimal**;
2. O _chunk_ em si.

Não podemos simplesmente ler a 2ª linha pois ela pode conter mais do que apenas
o conteúdo do _chunk_ (às vezes algum espaçamento extra para alinhar todos os
_chunks_ de uma maneira otimizada para transferência de dados, por exemplo), então temos que:

1. Ler o tamanho do _chunk_ na 1ª linha:

   ```python
       chunk_size = int(line, 16)
   ```

    O `16` serve para que a conversão para `int` leve em consideração de que
    `line` esteja em uma representação hexadecimal.
2. Se o tamanho do próximo _chunk_ for 0, então significa que terminamos a
   leitura:

   ```python
       if chunk_size == 0:
           return
   ```
3. Se não for, então lemos os próximos `chunk_size` bytes da resposta:

   ```python
       yield io.read(chunk_size)
   ```
4. E então ignoramos o que sobrar até a próxima linha:

   ```python
       io.readline()
   ```

Nosso `read_chunks` então ficará:

```python
def read_chunks(io: IO[bytes]) -> Iterable[bytes]:
    while True:
        line = io.readline().strip().decode()
        chunk_size = int(line, 16)

        if chunk_size == 0:
            return

        yield io.read(chunk_size)
        io.readline()
```

### 4º Passo: decodificar os dados recebidos

Tendo passado por tudo aquilo para receber os dados, finalmente chegamos na
etapa de descobrir como eles estão codificados e, então, decodificá-los (para
podermos lê-los com algum sentido). Para detectar qual a codificação, podemos
criar uma função `get_charset(headers, default)`. Precisaremos especificamente
do _header_ `Content-Type` para descobrir a codificação e, se ele não estiver
presente ou seu valor não informar a codificação, então retornamos o valor
passado em `default`:

```python
def get_charset(headers: Headers, default: str) -> str:
    content_type_header = headers['Content-Type']
```

O `Content-Type` pode vir só como `<tipo-do-conteúdo>`, mas também pode vir
na forma `<tipo-do-conteúdo>; charset=<codificação>`. Como só nos interessa o
`charset` (que sempre ficará do lado direito), podemos aplicar um `split('; ')`
e ignorar o que estiver mais à esquerda (caso tenha mais de um elemento):

```python
    _, *maybe_charset = content_type_header.split('; ')
```

A operação acima é chamada de _unpacking_, que pega algo como `a, b = (1, 2)` e
faz com que o valor de `a` seja 1 e `b` seja 2. Para este caso, o `*` aqui é
quem fará a parte mais mágica:
- Se `Content-Type` for `<tipo-do-conteúdo>`, então o `split` terá gerado
  apenas 1 elemento (afinal não teve o que aplicar `split` na _string_, pois
  não havia um `; `), e então `_` terá o valor `<tipo-do-conteúdo>` e
  `maybe_charset` será uma lista vazia (`[]`). Se não usássemos o `*`, esse
  caso daria um erro pois, em um caso `a, b = (value)` (ou seja, a tupla tem
  apenas um valor), isso causa um erro por tentar desempacotar 1 valor para 2
  variáveis.
- Se `Content-Type` for `<tipo-do-conteúdo>; charset=<codificação>`, então `_`
  será `<tipo-do-conteúdo>`, enquanto `maybe_charset` será
  `charset=<codificação>`.

Sendo assim, se `maybe_charset` não estiver vazio, então o `charset` está
definido e basta extraí-lo. Do contrário, utilizamos o _charset_ `default`:

```python
    if not maybe_charset:
        return default

    charset, = maybe_charset

    return charset.lstrip('charset=')
```

O `lstrip` apenas aplica um _strip_ do lado esquerdo da _string_, e no caso
estamos dizendo para ele cortar o que for `charset=` (em vez de espaços em
branco, que é o padrão de _strip_).

Isso finaliza nosso `get_charset`, e com isso podemos apenas completar nossa
`fetch_response` utilizando ele para decodificar o `body` (adiquirido no passo
anterior) e retornar o valor da função:

```python
def fetch_response(io: IO[bytes]) -> Response:
    code =  int(io.readline().split()[1])

    headers = fetch_headers(io)

    body = fetch_body(io, headers)

    charset = get_charset(headers, default='utf-8')
    return Response(
        headers,
        body.decode(charset) if body is not None else None
    )
```

Utilizando `fetch_response`
---------------------------

Em nosso `if __name__ == [...]` tínhamos:

```python
if __name__ == '__main__':
    with socket.create_connection((hostname, 80)) as sock:
        sock_out = sock.makefile('wb')

        send_request(sock_out, Request())
```

Para receber a resposta do servidor com nosso `fetch_response`, primeiro
precisamos criar um _socket_ de leitura:

```python
        # (Adicione acima de `sock_out = sock.makefile('rb')`)
        sock_in = sock.makefile('rb')
```

E, após enviarmos a requisição, aí sim receber a resposta utilizando
`fetch_response`:

```python
        # (Adicione abaixo de `send_request(sock_out, Request())`)
        response = fetch_response(sock_in)

        print(f'Código: {response.code}')
        print(f'Headers: {response.headers}')
        print(f'Conteúdo: {response.data}')
```

E pronto! Apesar de não lidarmos com recebimento de dados com compressão nem
com segurança via HTTPS, já temos um protocolo HTTP básico funcional. A partir
daqui, o que um _browser_ faria é aproveitar `response.data` para criar todos
os elementos gráficos, incluir códigos JavaScript, etc, enquanto um
[crawler](https://en.wikipedia.org/wiki/Web_crawler) vasculharia o código HTML
puro buscando informações, e por aí vai.


Código final desta seção
------------------------

```python
import socket
import sys
from dataclasses import dataclass, field
from typing import IO, Callable, Dict, Iterable, List, Optional, Tuple

Headers = Dict[str, str]


@dataclass(frozen=True)
class Response:
    headers: Headers
    data: Optional[str]


@dataclass(frozen=True)
class Request:
    headers: Dict[str, str] = field(default_factory=lambda: {})
    data: Optional[str] = None
    method: str = 'GET'


def read_headers(io: IO[bytes]) -> Iterable[Tuple[str, str]]:
    while True:
        line = io.readline().strip()
        if not line:
            return

        key, value = line.decode().split(': ', maxsplit=1)
        yield key, value


def fetch_headers(io: IO[bytes]) -> Headers:
    io.readline()

    return dict(read_headers(io))


def read_chunks(io: IO[bytes]) -> Iterable[bytes]:
    while True:
        line = io.readline().strip().decode()
        chunk_size = int(line, 16)

        if chunk_size == 0:
            break

        yield io.read(chunk_size)
        io.readline()


def get_chunked_body(io: IO[bytes]) -> bytes:
    return b''.join(read_chunks(io))


def get_charset(_headers_: Headers, default: str) -> str:
    content_type_header = headers['Content-Type']

    _, *maybe_charset = content_type_header.split('; ')

    if not maybe_charset:
        return default

    charset, = maybe_charset

    return charset.lstrip('charset=')


def fetch_body(io: IO[bytes], headers: Headers) -> Optional[bytes]:
    if 'Content-Length' in headers:
        length = int(headers['Content-Length'])
        return io.read(length)

    if (
        'Transfer-Encoding' in headers and
        headers['Transfer-Encoding'] == 'chunked'
    ):
        return get_chunked_body(io)

    return None


def fetch_response(io: IO[bytes]) -> Response:
    headers = fetch_headers(io)

    body = fetch_body(io, headers)

    charset = get_charset(headers, default='utf-8')
    return Response(
        headers,
        body.decode(charset) if body is not None else None
    )


def send_request(io: IO[bytes], request: Request):
    io.write(f'{request.method} / HTTP/1.1\n'.encode())

    for header, value in request.headers.items():
        io.write(f'{header}: {value}\n'.encode())

    if request.data is not None:
        io.write(f'{request.data}\n'.encode())

    io.write(b'\n')

    io.flush()

if __name__ == '__main__':
    hostname = args[1]

    with socket.create_connection((hostname, 443)) as sock:
        sock_in = sock.makefile('rb')
        sock_out = sock.makefile('wb')

        send_request(sock_out, Request())

        response = fetch_response(sock_in)

        print(f'Código: {response.code}')
        print(f'Headers: {response.headers}')
        print(f'Conteúdo: {response.data}')
```
