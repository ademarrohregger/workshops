TCP/UDP
=======

TCP
---

Em resumo, TCP serve para se ter algumas garantias quanto aos pacotes
enviados/recebidos em uma conexão, como:
- Garantia de que os dados foram todos entregues;
- Garantia de que é possível remontar os pacotes na ordem correta (afinal, pode
  ser que alguns pacotes sejam enviados mais rápido que outros);
- Garantia de que os dados foram recebidos integralmente (não aconteceu de
  algum deles se corromper).

Isso é feito adicionando algumas informações extras nos pacotes (além dos
próprios dados que se quer enviar), como: qual o nº de um pacote na sequência
que se está enviando, alguns bits extras para checagem de integridade (há um
pequeno algoritmo que é executado em cima dos dados recebidos, em que se espera
que o resultado seja uma sequência de bits que bata com os bits extras - se não
baterem, então algum dado se corrompeu durante a transmição do pacote).

Isso, é claro, tem seu custo: pacotes podem acabar sendo reenviados, há todo o
processo de se garantir a integridade dos pacotes, e tudo isso acaba por fazer
a comunicação não ser tão eficiente.

> São decisões ótimas para garantir que a sua página HTML esteja montada da
> forma correta como se espera, porém são péssimas quando a sua preocupação é
> fazer uma chamada de vídeo em tempo-real com seu amigo que está em outro
> país.

UDP
---

Em contraste com o TCP, o UDP não faz todos os passos para se garantir uma
conexão íntegra e bem formada. Em vez disso, a única preocupação do UDP é: os
dados foram enviados e pronto. Não fazer todos os procedimentos extras de
garantia do TCP, especialmente na questão de reenviar pacotes perdidos, faz com
que UDP não sofra tanto com perda de desempenho em uma conexão mais lenta. Em
vez disso, se tem perda de qualidade do áudio ou vídeo, por exemplo, o que
torna UDP mais apropriada para _streaming_ de mídia. Em uma vídeo chamada, por
exemplo, se está interessado nos dados em tempo real, ou seja: eles são úteis
por pouco tempo. Logo, se um pacote foi perdido, o tempo de se reenviar esse
pacote não compensará, já que passaria do prazo de utilidade dele.

> A preferência por performance e bom uso da rede acima de integridade se torna
> ótima para streaming de vídeo, áudio, vídeo-conferência, mas é péssima para
> conteúdos sensíveis a erros (como HTML).

Uma leve ressalva: em uma conexão local, a perda de pacotes costuma ser baixa,
então utilizar TCP para _streaming_ de mídia é viável nesse caso.
