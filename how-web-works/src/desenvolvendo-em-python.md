Como são os ambientes de produção com Python?
=============================================

Quando se está desenvolvendo um projeto em Python, é interessante que se tenha:
- Um **Gerenciador de Dependências**, que irá garantir que, dentro daquele
  projeto, todas as dependências com suas versões específicas para ele estão
  instaladas. Assim é possível ter mais controle sobre possíveis problemas, uma
  vez que você já indica "esta biblioteca funciona para a versão X da
  dependência Y";
- Uma **Virtualenv**, que isola os pacotes instalados no sistema dos pacotes
  instalados para o projeto em desenvolvimento. Por exemplo, pode ser que no
  seu computador você tenha Qt5.12 instalado (para executar outros programas,
  como editor de vídeo), porém seu projeto trabalha com Qt5.8. Como não ter
  conflito entre o Qt do seu sistema com o Qt do seu projeto? Você instala o
  Qt5.8 na Virtualenv do projeto, e assim ele garantidamente não interfere no
  Qt5.12 do sistema. Bons gerenciadores de dependência, ao mandar instalar as
  dependências, criam uma Virtualenv para seu projeto e instalam as
  dependências nela (e não no sistema).


Ferramentas que utilizaremos
----------------------------


Como gerenciador de dependências, utilizaremos **Poetry**. O Poetry, inclusive,
é um dos bons gerenciadores que já cria e utiliza uma Virtualenv separada para
seu projeto. A parte de dependências não será utilizada no início, mas mais pro
final, quando formos trabalhar com conteúdo fornecido com alguma compressão.

Como editor de código, esse fica por sua escolha.

Passos gerais
-------------

1. Criaremos o projeto da biblioteca com o Poetry;
2. Faremos a implementação de um HTTP básico (emitindo requisições e recebendo
   respostas);
3. Adicionaremos suporte a criptografia (HTTPS);
4. Adicionaremos suporte a conteúdo comprimido (instalando algumas dependências
   para isso).
