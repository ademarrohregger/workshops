import socket
from dataclasses import dataclass
from typing import Dict, Optional

# Define um tipo "Headers" como um dicionário de strings -> strings.
Headers = Dict[str, str]


# Define como são estruturadas as requisições
@dataclass(frozen=True)
class Request:
    headers: Headers = field(default_factory=lambda: {})
    data: Optional[str] = None
    method: str = 'GET'


def send_request(io, request):
    pass


if __name__ == '__main__':
    with socket.create_connection(('google.com', 80)) as sock:
        print('Conexão estabelecida.')
