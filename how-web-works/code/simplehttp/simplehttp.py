import gzip
import socket
import ssl
import sys
import zlib
from dataclasses import dataclass, field
from pprint import pprint, pformat
from typing import IO, Callable, Dict, Iterable, List, Optional, Tuple

import brotli

Headers = Dict[str, str]


@dataclass(frozen=True)
class Response:
    code: int
    headers: Headers
    data: Optional[str]


@dataclass(frozen=True)
class Request:
    headers: Dict[str, str] = field(default_factory=lambda: {})
    data: Optional[str] = None
    method: str = 'GET'


def read_headers(io: IO[bytes]) -> Iterable[Tuple[str, str]]:
    while True:
        line = io.readline().strip()
        if not line:
            return

        key, value = line.decode().split(': ', maxsplit=1)
        yield key, value


def fetch_headers(io: IO[bytes]) -> Headers:
    return dict(read_headers(io))


def read_chunks(io: IO[bytes]) -> Iterable[bytes]:
    while True:
        line = io.readline().strip().decode()
        chunk_size = int(line, 16)

        if chunk_size == 0:
            break

        yield io.read(chunk_size)
        io.readline()


def get_chunked_body(io: IO[bytes]) -> bytes:
    return b''.join(read_chunks(io))


def get_charset(headers: Headers, default: str) -> str:
    content_type_header = headers['Content-Type']

    _, *maybe_charset = content_type_header.split('; ')

    if not maybe_charset:
        return default

    charset, = maybe_charset

    return charset.lstrip('charset=')


def fetch_body(io: IO[bytes], headers: Headers) -> Optional[bytes]:
    if 'Content-Length' in headers:
        length = int(headers['Content-Length'])
        return io.read(length)

    if (
        'Transfer-Encoding' in headers and
        headers['Transfer-Encoding'] == 'chunked'
    ):
        return get_chunked_body(io)

    return None


DECOMPRESSORS: Dict[str, Callable[[bytes], bytes]] = {
    'gzip': gzip.decompress,
    'deflate': zlib.decompress,
    'br': brotli.decompress,
}


def fetch_response(io: IO[bytes]) -> Response:
    code =  int(io.readline().split()[1])

    headers = fetch_headers(io)

    body = fetch_body(io, headers)

    if 'Content-Encoding' in headers:
        assert body is not None
        body = DECOMPRESSORS[headers['Content-Encoding']](body)

    charset = get_charset(headers, default='utf-8')
    return Response(
        code,
        headers,
        body.decode(charset) if body is not None else None
    )


def send_request(io: IO[bytes], request: Request):
    io.write(f'{request.method} / HTTP/1.1\n'.encode())

    for header, value in request.headers.items():
        io.write(f'{header}: {value}\n'.encode())

    if request.data is not None:
        io.write(f'{request.data}\n'.encode())

    io.write(b'\n')

    io.flush()


def test_request(s: socket.socket):
    sock_in = s.makefile('rb')
    sock_out = s.makefile('wb')

    send_request(
        sock_out,
        Request(
            headers={
                'Accept-Encoding': 'gzip',
                'User-Agent':
                    'Mozilla/5.0 (X11; Linux x86_64) '
                    ' AppleWebKit/537.36 (KHTML, like Gecko) '
                    'Chrome/74.0.3729.157 Safari/537.36',
            }
        )
    )

    response = fetch_response(sock_in)

    print(f'Code: {pformat(response.code)}')
    print(f'Headers: {pformat(response.headers)}')
    print(f'Data: {pformat(response.data)}')


def main(args: List[str]):
    context = ssl.create_default_context()

    hostname = args[1]

    with socket.create_connection((hostname, 443)) as sock:
        with context.wrap_socket(sock, server_hostname=hostname) as s:
            test_request(s)


if __name__ == '__main__':
    main(sys.argv)
