#!/usr/bin/env bash
EXEC_NAME=$(basename $0)
VERSION="v0.0.1"

OPERATIONS=":generate:"

USAGE="Usage: ${EXEC_NAME} <operation> <filename>
       ${EXEC_NAME} -h
       ${EXEC_NAME} -v

Operations:
    generate        Generates code from template file."


show-usage() {
    local operation="$1"

    local op_usage="Usage for \"${operation}\" operation:"

    case "${operation}" in
        generate)
            echo "${op_usage}"
            echo "    ${operation} <template-file> <output> <...template-args>"
            ;;
        *)
            echo "${USAGE}"
    esac

}


show-version() {
    echo "CodeGenerator ${VERSION}"
}


die() {
    local message="${1}";
    if test -z "${message}"
    then
        echo "Fatal error."
    else
        echo "Fatal error: ${message}"
    fi
    exit 1
}


die-if-invalid-op() {
    if [[ ":${OPERATIONS}:" != *":${OPERATION}:"* ]]
    then
        die "Invalid operation \"${OPERATION}\"."
    fi
}


die-if-none() {
    local variable="$1"
    local message="$2"
    if test -z "${variable}"
    then
        show-usage "${OPERATION}"
        die "${message}"
    fi
}


parse-args() {
    while getopts ":hv" option
    do
        case "${option}" in
            h)
                show-usage
                exit 0
                ;;
            v)
                show-version
                exit 0
                ;;
            *)
                show-usage
                die "Unknown option \"${OPTARG}\""
        esac
    done

    if test -z "$1"
    then
        show-usage
        die "Missing operation."
    fi

    OPERATION="$1"
    die-if-invalid-op

    shift 1

    OPARGS=$*
}


generate() {
    local template="$1"
    local output="$2"
    shift 2

    die-if-none "${template}" "Missing template filename."
    die-if-none "${output}" "Missing output filename."

    local template_args="$*"
    local argregex="([a-zA-Z0-9_]+)=([a-zA-Z0-9_]+)"

    echo "Starting generation process."

    cp ${template} ${output}
    for arg in ${template_args}
    do
        if [[ $arg =~ $argregex ]]
        then
            local placeholder="${BASH_REMATCH[1]}"
            local value="${BASH_REMATCH[2]}"

            echo "Arg: ${placeholder} = ${value}"
            sed -i "s/\${${placeholder}}/${value}/g" ${output}
        fi
    done

    echo "Done."
}


main() {
    parse-args ${ARGS} || die
    ${OPERATION} ${OPARGS} || die
}

ARGS="$@"
main
