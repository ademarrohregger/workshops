Oficinas de Verão - Fevereiro 2019
==================================

1ª Semana
---------

### Palestra: Carreira QA

Palestrante: Vanessa Cunha

- [Slides](https://docs.google.com/presentation/d/11Q4GoxenpwBiKCOeJRzp8vfbVCUyRK6rkwk-KMMzL5w/edit?usp=sharing)

### Oficina: Postman para Iniciantes

Ministrante: Vanessa Cunha

- [Slides](https://docs.google.com/presentation/d/17ZAYfXSZhcOcMRyjYdj2uYfKSKD3xRDr3cggYFao7z0/edit?usp=sharing)

### Oficina: Versionamento com Git

Ministrante: João Paulo TIZ

- [Guia rápido de comandos Git](https://pet-comp-ufsc.github.io/tutorials/tools/recommended/git.html)
- [Git-Book](http://git-scm.com/book)
- [(Jogo) Learn Git Branching](http://learngitbranching.js.org)

---

2ª Semana
---------

### Palestra: Carreira em Segurança

Palestrante: Tauane de Jesus

- [Slides](./res/carreira-em-segurança.pdf)

### Oficina: O Ambiente Node.JS

Ministrante: Lucas Muller

- [Slides](https://docs.google.com/presentation/d/1w6ucFnbr5XGzp9bSAGsX20SWTDNuYi_-rl9UGmKMOZw/edit?usp=sharing)
- Cursos da Udemy recomendados:
    - [The Complete JavaScript Course 2019: Build Real Projects!](https://www.udemy.com/the-complete-javascript-course/)
    - [The Complete Node.js Developer Course (3rd Edition)](https://www.udemy.com/the-complete-nodejs-developer-course-2/)
    - [Node JS: Advanced Concepts](https://www.udemy.com/advanced-node-for-developers/)

### Oficina: O Framework Express

Ministrante: Lucas Muller

- [Slides](https://slides.com/lucasflomuller/deck#/)
- Cursos da Udemy recomendados:
    - [Just Express (with a bunch of node and http). In detail.](https://www.udemy.com/just-express-with-a-bunch-of-node-and-http-in-detail/)

---

3ª Semana
---------

### Oficina: Conhecendo ShellScript

Ministrante: João Paulo TIZ

- ["Shell Scripting Tutorial"](https://www.shellscript.sh/index.html)
- Livro "ShellScript Profissional":
    - [NovaTec](https://www.novatec.com.br/livros/shellscript/)
    - [Amazon](https://www.amazon.com.br/Script-Profissional-Aurelio-Marinho-Jargas/dp/8575221523)

### Oficina: Criando documentos bonitos com LaTeX

Ministrante: João Paulo TIZ

- [Tutoriais de LaTeX](https://pet-comp-ufsc.github.io/tutorials/langs/latex/)
- [Documentação do Overleaf](https://www.overleaf.com/learn/latex/Free_online_introduction_to_LaTeX_(part_1))
- ["Introdução ao LaTeX2e"](http://mirrors.ctan.org/info/lshort/portuguese-BR/lshortBR.pdf) (recomendado)
