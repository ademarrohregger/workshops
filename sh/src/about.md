O que é ShellScript
===================

ShellScripts são programas feitos para executar comandos POSIX. Na prática, são
utilizados para automatizar várias das tarefas que distribuições Linux devem
executar, ou ainda as tarefas das etapas de desenvolvimento de um software
(chamar compiladores, linkers, executar testes...).

Para exercitar e aprender a criar seu próprio ShellScript, nesta oficina será
criado um **gerador de código**.
