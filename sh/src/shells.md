Sh, Bash, Zsh, Fish, ...
========================

Há diferentes variações de Shells, cada uma com recursos diferentes.

O shell padrão de sistemas Unix foi inicialmente o Bourne Shell (`sh`), do qual
eventualmente surgiram as outras variações:

- Bourne-Again Shell: `bash`;
- Z-Shell: `zsh`;
- Friendly Interactive Shell: `fish`.

E a lista vai longe.

Por questões de maior compatibilidade, nesta oficina será utilizado o **Bash**,
que diferente dos outros dois (`zsh` e `fish`) costuma vir instalado por padrão
em distribuições Linux e é mais completo do que o `sh`.
