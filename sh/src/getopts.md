getopts
=======

```bash
#!/usr/bin/env bash
EXEC_NAME=$(basename $0)
VERSION="v0.0.1"

USAGE="Usage: ${EXEC_NAME} <operation> <filename>
       ${EXEC_NAME} --help
       ${EXEC_NAME} --version
"

if [[ $1 == "create" ]];
then
    echo "Creating ${FILENAME}"
elif [[ $1 == "edit" ]];
then
    echo "Editing ${FILENAME}"
elif [[ $1 == "--help" ]];
then
    echo $USAGE
elif [[ $1 == "--version" ]];
then
    echo "My project ${VERSION}"
else
    echo "Missing operation!"

    echo $USAGE
fi
```
