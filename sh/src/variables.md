Variáveis
=========

#### my-script.sh

```bash
#!/usr/bin/env bash

echo "Args: $*"

FILENAME="$2"

if [[ $1 == "create" ]];
then
    echo "Creating ${FILENAME}"
elif [[ $1 == "edit" ]];
then
    echo "Editing ${FILENAME}"
else
    echo "Missing operation!"
fi
```
