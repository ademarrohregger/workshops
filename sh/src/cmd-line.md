A Linha de Comando
==================

Linhas de comando de Shell são, no fundo no fundo, interpretadores interativos
de suas respectivas linguagens Shell (ou seja, você escreve os comandos daquele
Shell um por um em vez de rodar um script pronto como programa).

Ao iniciar uma sessão em um terminal, a primeira ação do terminal é abrir o
interpretador interativo do Shell padrão do usuário. Nele, então, é possível
digitar os comandos a serem executados:

```console
$ echo "Hello, terminal!"
Hello, terminal!
```

Comandos úteis
--------------

### Mostrar mensagem

```console
$ echo Texto
Texto
```

### Ver conteúdo de uma pasta

```console
$ ls
readme.md  sample.py

$ ls -lh   # -l: mostrar em lista; -h: mostrar peso em K, M, G
total 8.0K
-rw-r--r-- 1 user user 100 Feb 14 07:45 readme.md
-rw-r--r-- 1 user user  29 Feb 14 13:16 sample.py

$ ls <pasta específica> # Para buscar em uma pasta específica que não seja a
                        # atual
```

### Trocar de pasta

```console
$ cd <pasta>
```

Retornar à pasta anterior:

```console
$ cd ..
```
